import base_options, stationary_source

if __name__ == '__main__':
    while True:
        try:
            args = base_options.input_data()
            break
        except Exception as err:
            print(err)

    try:
        in_file = open(args[1], 'r', encoding='utf-8')
        out_file = args[2]
        source = stationary_source.StationarySource(in_file)
        if args[0] == 1:
            result = source.getEntropy()
        else:
            result = []  # type: list
            for model in source.models:
                result.append(source.gen_encoding(args[3], args[4], args[5], model))

        base_options.output_data(out_file, result)

    except Exception as err:
        print('Error:\n')
        print(err)
