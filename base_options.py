import argparse
import json

def input_data():
    mode = 0  # type: int
    input_file = ''  # type: str
    output_file = ''  # type: str
    r = 0  # type: float
    e = 0  # type: float
    q = 0  # type: int

    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', help="This is a working mode")
    parser.add_argument('--input', help="Input file name")
    parser.add_argument('--output', help='Output file name')
    parser.add_argument('--r', type=float, help='Encoding rate')
    parser.add_argument('--e', type=float, help='Decoding error boundary')
    parser.add_argument('--q', type=int, help='Encoder alphabet power')

    args = parser.parse_args()
    if args.mode:
        if args.input:
            input_file = args.input
        else:
            raise Exception('No input file specified!')

        if args.output:
            output_file = args.output

        if args.mode == '1':
            mode = 1
            return [mode, input_file, output_file]
        elif args.mode == '2':
            mode = 2

            if args.r:
                r = args.r
            else:
                raise Exception('No encoding rate \'R\' specified!')

            if args.e:
                e = args.e
            else:
                raise Exception('No decoding error boundary \'e\' specified!')

            if args.q:
                q = args.q
            else:
                raise Exception('No encoder alphabet power \'q\' specified!')

            return [mode, input_file, output_file, r, e, q]
        else:
            raise Exception('Invalid mode!')

    else:
        print('Please, select a working mode:')
        print('1   -    Calculation of the source\'s own entropy;')
        print('2   -    Calculation of the code.')

        while True:
            tmp = input()
            if tmp == '1':
                mode = 1

                input_file = input('Please, specify the input file')
                output_file = input('Please, specify the output file')

                return [mode, input_file, output_file]
            elif tmp == '2':
                mode = 2

                input_file = input('Please, specify the input file')
                output_file = input('Please, specify the output file')

                try:
                    r = float(input('Please, specify the encoding rate'))

                    e = float(input('Please, specify the decoding error boundary'))

                    q = int(input('Please, specify the encoder alphabet power'))
                except Exception as err:
                    raise Exception(err)

                return [mode, input_file, output_file, r, e, q]

def output_data(filename, data):
    if filename:
        with open(filename, 'w', encoding='utf-8') as outfile:
            json.dump(data, outfile, ensure_ascii=False)
    else:
        print(data)
