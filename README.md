# stationary_source

**Кодирование стационарных источников без памяти равномерными кодами.**

В программе присутствуют два режима работы.

**Режим 1.** На вход подаётся описание стационарного источника без памяти в формате json.

    Программа выдаёт значения собственной энтропии источников, заданных в файле.

**Режим 2.** На вход подаётся

    1. Описание стационарного источника,

    2. Скорость кодирования ,

    3. Граница ошибки декодирования ,

    4. Мощность алфавита кодера.

    На выход подается json файл/печатается в терминал файл с кодом и кодированием для всех перечисленных источников.

**Использование:**

    В корне каталога запускается `python3 stationary_source.py`. Также возможен запуск программы и передача в неё входных данных 
    
    с использованием командной строки, подробности - `python3 stationary_source.py --help`. Если нужен вывод результатов на экран,
    
    а не в файл, опускается флаг `--output`.
