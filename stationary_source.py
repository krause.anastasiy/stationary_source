import json
import fractions
import math

class StationarySource:
    models = {}  # type: dict
    distribs = {}  # type: dict
    entropies = {}  # type: dict

    def __init__(self, file):
        source = json.load(file)
        self.models = source['models']
        for model in self.models:
            self.distribs[model] = self.calcProbDistr(model)

    def calcProbDistr(self, model):
        distrib = {}  # type: dict
        check_sum = 0  # type: float
        for symbol in self.models[model]:
            distrib[symbol] = float(fractions.Fraction(self.models[model][symbol]))
            check_sum += float(fractions.Fraction(self.models[model][symbol]))

        if check_sum != 1:
            raise Exception('Error: incorrect probability distribution!')

        return distrib

    def getEntropy(self):
        entropies = []  # type: list
        for model in self.models:
            entropy = 0  # type: float
            for prob in self.distribs[model]:
                entropy -= math.log2(self.distribs[model][prob]) * self.distribs[model][prob]
            self.entropies[model] = entropy
            entropies.append(entropy)

        return entropies

    def getSet(self, e, n, model):
        set = []  # type: list
        for i in range(2**n):
            num_of_units = bin(i).count('1')
            prob = self.distribs[model]['1'] ** num_of_units * self.distribs[model]['0'] ** (n - num_of_units)
            info = - math.log2(prob) / n
            self.getEntropy()
            if math.fabs(info - self.entropies[model]) <= e:
                set.append(i)

        return set

    def calc_prob(self, hps, model, n):  # не работает, ибо >= 1
        p = 0
        for elem in hps:
            cur_p = 1
            for symbol in self.models[model]:
                bin_set = format(elem, '0' + str(n) + 'b')
                cur_p *= self.distribs[model][symbol] ** bin_set.count(symbol)
            p += cur_p

            if p > 1:
                raise Exception('Bad probability')
        return p

    def gen_encoding(self, R, eps, q, model):
        data = {}  # type: dict
        n = 1
        while True:
            set = self.getSet(eps, n, model)

            if set:
                if R >= self.entropies[model] or R <= 0:
                    raise Exception('Invalid R! Model ' + model)
                if self.calc_prob(set, model, n) >= 1 - (self.entropies[model] - R):
                    code = []
                    size = 1
                    while q ** size < len(set):
                        size += 1
                    for i in range(len(set)):
                        word = ""
                        elem = i
                        for j in range(size):
                            word = str(elem % q) + word
                            elem = elem // q
                        code.append(word)

                    tmp = {}  # type: dict
                    for i in range(len(code)):
                        tmp[format(set[i], '0' + str(n) + 'b')] = code[i]
                    data[model] = {
                        "code": code,
                        "encoding": tmp
                    }
                    return data
            n += 1
